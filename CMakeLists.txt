cmake_minimum_required(VERSION 3.26)
project(test_modules)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_VERBOSE_MAKEFILE ON)
add_compile_options(-fmodules-ts)

message("PLOP " ${CMAKE_CXX_COMPILER_VERSION})

#add_subdirectory(Foo)

add_executable(test_modules main.cpp Foo/foo.cpp)
