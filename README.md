# Requirements
- GCC 11
- CMake 3.26

# Build
Either: 
- cmake .
- g++ -std=c++20 -fmodules-ts -x c++ foo.ixx main.cpp -o main
