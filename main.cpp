#include <iostream>

import foo;

int main() {
    std::cout << "Hello, World!" << std::endl;
    Foo::Foo foo;
    foo.foo();
    return 0;
}
