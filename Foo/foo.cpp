//
// Created by marechaljas on 31/08/23.
//
module;

#include <iostream>
//import std; //C++23
export module foo;
export namespace Foo {
    class Foo {
    public:
        void foo() {
            std::cout << "Foo" << std::endl;
        }
    };
}
